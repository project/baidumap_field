
CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------

This module  allows content creators/maintainers to add maps to content via
the addition of a "Baidu Map Field" field type that can be added to content
types.

Using the Baidu map field, the user can drop a marker on a map, set the
width, height and zoom factor of the map and save the data for the map
with the node.

INSTALLATION
------------

Install as usual...
see http://drupal.org/documentation/install/modules-themes/modules-7

Usage
-----
The Baidumap module provides a field to display Baidu Map.

1) First, you have to apply a Baidu map ak from http://lbsyun.baidu.com/.
  Navigate to the service administration  Administration -> Configuration ->
  Web services -> Baidu Map Field API Key
  (admin/config/services/baidu-map-field-apikey). Input you Baidu map ak to the
  form and save.

2) Create an Baidumap field on your Drupal site if you have not already.
  Administration -> Structure -> Types (admin/structure/types).


Author
-------
James Wang
